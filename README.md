![Logo](../logo_cut.png)

# MindMap Project

## Overview
The MindMap project aims to introduce a MindMap feature into the existing Zettelstore to clearly visualize the dependencies and links between notes. 
This will be the first time the Zettelstore is provided on a Linux server to enable connection from various devices. 
The project involves creating multiple notes with different content and integrating them into the Zettelstore. Additionally, 
an interface to FreeMind will be implemented to establish links between the notes in a MindMap.

## Project Goals
The main goal of this project is to integrate a functional MindMap feature into the existing Zettelstore by January 10, 2024, 
to clearly visualize the dependencies and connections of notes.

## Roadmap / Milestones

- **M1 (04.10.23):** Group formation.
- **M2 (04.10.23):** Sprint 1: Create project assignment.
- **M3 (11.10.23):** Submission of the project assignment.
- **M4 (11.10.23):** Submission of requirements description.
- **M5 (11.10.23 - 25.10.23):** Sprint 2: Zettelstore matched with Linux-Server.
- **M6 (11.10.23 - 25.10.23):** Project initiation.
- **M7 (25.10.23 - 08.11.23):** Sprint 3: Content generation.
- **M8 (08.11.23 - 22.11.23):** Sprint 4: Implementation of the interface to FreeMind.
- **M9 (22.11.23 - 13.12.23):** Sprint 5: Testing.
- **M10 (13.12.23 - 10.01.24):** Sprint 6: Preparation of the presentation.
- **M11 (10.01.24):** Final presentation of the MindMap.
- **M12 (10.01.24):** Submission of the product.
## Risks and Mitigation Strategies
1. **Time Delays:** Plan buffer times and regularly review progress.
2. **Technical Difficulties:** Conduct early prototypes and tests.
3. **Security Risks:** Implement robust security protocols and encryption.
4. **Team Communication:** Hold regular meetings and clearly assign responsibilities.

## Budget Overview
- **Domain:** €0.93/year
- **Server:** €5.00/month for 4 months (Total: €20.00)
- **Total Estimated Cost:** €20.93

## Authors 

XXX Ändern! In richtige Namen

- [eyagar (Project Leader)](https://gitlab.win.hs-heilbronn.de/eyagar)
- [cnguyen](https://gitlab.win.hs-heilbronn.de/cnguyen)
- [dmchitar](https://gitlab.win.hs-heilbronn.de/dmchitar)
- [syedier](https://gitlab.win.hs-heilbronn.de/syedier)
- [lpepi](https://gitlab.win.hs-heilbronn.de/lpepi)
- [hakcay](https://gitlab.win.hs-heilbronn.de/hakcay)
- [mmese](https://gitlab.win.hs-heilbronn.de/mmese)

## Run on macOS

#### 1. Before you can run the project, ensure you have the following pre-requisites installed(must run locally):

- Homebrew (macOS Package Manager)
- Python 3.x
- Graphviz


Install **Homebrew** by running the following command:

```bash
  /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"

```

Make sure you have **Python 3.x** installed on your system. You can check your Python version by running:

```bash
  python3 --version
```
Run the following command to install **Graphviz** via Homebrew:

```bash
  brew install graphviz
```





#### 2. Once this prerequisite is installed you can start cloning the git repository.

Run git clone 

```bash
Run git clone https://gitlab.win.hs-heilbronn.de/dmchitar/swt-mindmap
```

Open the terminal where you saved the file and run that command

```bash
pip3 install -r requirements.txt
````

Next comes the last command 

```bash
pyhton3 main.py
```
Finally, enter an ID so that a PDF with the topic you have selected appears in the "doctest-output" folder 


## Run on Windows

#### 1. Before you can run the project, ensure you have the following pre-requisites installed(must run locally):

- winget (Windows Package Manager)
- Python 3.x
- Graphviz


Install **Python3** by running the following command:

```bash
winget install -e --id Python.Python.3.12

```

Make sure you have **Python 3.x** installed on your system. You can check your Python version by running:

```bash
  python3 --version
```
Run the following command to install **Graphviz** via Termnial:

```bash
  winget install -e --id Graphviz.Graphviz
```

After you have completed these steps, please restart your machine 

```bash
shutdown /r /t 0
```
#### 2. Once this prerequisite is installed you can start cloning the git repository.

Run git clone 

```bash
Run git clone https://gitlab.win.hs-heilbronn.de/dmchitar/swt-mindmap
```

Open the terminal where you saved the file and run that command

```bash
pip3 install -r requirements.txt
````

Next comes the last command 

```bash
pyhton3 main.py
```
Finally, enter an ID so that a PDF with the topic you have selected appears in the "doctest-output" folder 

## Run on Linux

asdasdasdasdasdasd

