# Diagraph settings

import graphviz
import os

os.environ["PATH"] += os.pathsep + '/opt/homebrew/Cellar/graphviz/9.0.0/bin'
dot = graphviz.Digraph(comment='The Round Table')

# Global graph attributes
dot.attr(rankdir='LR', size='15,15')

# Node attributes
dot.attr('node', shape='ellipse', style='filled', fillcolor='lightgrey', fontname='Helvetica', fontsize='10')

# Edge attributes
dot.attr('edge', color='blue', fontname='Helvetica', fontsize='10')
