# User Guide for the MindMap Project on macOS

## Table of Contents

1. **Introduction**
   - What is the MindMap Project?
   - Why Use the User Guide?
   
2. **Prerequisites**
   - Installing Python3
   - Installing Graphviz
   - Installing Other Required Packages
   
3. **Getting Started**
   - Cloning the Project Repository
   - Setting Up a Virtual Environment (recommended)
   
4. **Running the MindMap Application**
   - How to Launch the Application
   - Navigating the User Interface
   
5. **Creating a MindMap**
   - Step-by-Step Instructions
   - Adding and Linking Notes
   
6. **Exporting and Sharing MindMaps**
   - Exporting MindMaps as Image or PDF
   - Sharing MindMaps with Others
   
7. **Advanced Features**
   - Customizing MindMaps
   - Keyboard Shortcuts
   - Command-Line Options
   
8. **Troubleshooting**
   - Common Issues and Solutions
   - Reporting Bugs or Problems
   
9. **Contributing to the Project**
   - How to Contribute Code
   - How to Contribute to Documentation
   - Reporting Issues and Providing Feedback
   
10. **License**
    - 
   
11. **Acknowledgments**
    - Credits and Thanks to Contributors
   
12. **Contact Information**
    - How to Get in Touch with the Project Team
   
## Introduction

### What is the MindMap Project?

The MindMap Project is a tool for visualizing dependencies and links between notes, allowing you to create mind maps to organize your thoughts and ideas.

### Why Use the User Guide?

This user guide will help you get started with the MindMap Project on macOS. It provides step-by-step instructions, tips, and troubleshooting information to make your experience with the application smoother.

## Prerequisites

### Installing Python3

1. Open Terminal on your macOS.
2. Check if Python3 is already installed by running `python3 --version`. If not, you can download and install Python3 from [Python's official website](https://www.python.org/downloads/).

### Installing Graphviz

1. Install Homebrew (if not already installed) by following the instructions on [brew.sh](https://brew.sh/).

2. Open Terminal and run the following command to install Graphviz:

```shell
brew install graphviz
```

### Installing other required packages
```shell
pip3 install -r requirements.txt
```
