import requests
from querry import create_mindmap, synchronize_zettel
import json

headers = {
    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
    'Upgrade-Insecure-Requests': '1',
    'Connection': 'keep-alive',
}

all_zettel = requests.get('http://zettel.store:23123/z', headers=headers)

idList = []

for zettel_id in all_zettel.text.split("\n"):
    idList.append(zettel_id[:14])
idList.sort()
filtered_idList = list(filter(None, idList))

plain_data = requests.get('http://zettel.store:23123/z?_seed=6387&enc=data')
synchronize_zettel(plain_data.text, filtered_idList)

ownInput = input("Enter ID: ")

json_filename = 'data/structured.json'

with open(json_filename, 'r') as json_file:
    meta_data = json.load(json_file)

create_mindmap(ownInput, meta_data)

