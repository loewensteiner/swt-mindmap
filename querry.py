import requests
import mmgraphviz
import json
import os


def key_picker(meta_key):
    """Key(s) filter."""

    return any(keyword in meta_key for keyword in ["title:", "superior:", "subordinates:", "folge:"])


def filter_meta(zettel_id):
    """Fetch and filter metadata for a given zettel."""

    zettel_meta = requests.get(f'http://zettel.store:23123/z/{zettel_id}?enc=zmk&part=meta')
    zettel_data = zettel_meta.text.split('\n')
    filtered_meta_list = list(filter(None, (filter(key_picker, zettel_data))))

    return filtered_meta_list


def format_meta_data(meta_data):
    """Structure data into JSON format."""

    meta_dict = {}

    for pair in meta_data:
        meta_key = pair.split(": ")[0]
        meta_value = pair.split(": ")[1]

        if meta_key != "title" and len(meta_value.split(" ")) > 1:
            meta_dict[meta_key] = meta_value.split(" ")
        else:
            meta_dict[meta_key] = meta_value

    return meta_dict


def save_meta_to_json(formated_dict, filename='structured.json'):
    """Save data into JSON."""

    file_path = os.path.join('data', filename)
    with open(file_path, 'w') as json_file:
        json.dump(formated_dict, json_file, indent=4)


def synchronize_zettel(zettel_data, id_list):
    """
    Save zettel data as both plain text and structured JSON
    and synchronize data if there are updates/changes.
    """

    folder_name = 'data'
    file_plain = os.path.join(folder_name, 'plain.txt')

    # Check if the folder exists, if not create it
    if not os.path.exists(folder_name):
        os.makedirs(folder_name)

    # Check if the file exists, if not create it add plain data from zettel store
    # Check if there are updates on zettel store, if so, update plain data
    if not os.path.exists(file_plain) or open(file_plain).read() != zettel_data:
        with open(file_plain, 'w') as file:
            file.write(zettel_data)

            structured_zettel_dict = {}
            for zettel in id_list:
                structured_meta = format_meta_data(filter_meta(zettel))
                structured_zettel_dict[zettel] = structured_meta

            save_meta_to_json(structured_zettel_dict)


def uplink_relatives(zettel, data, filename):
    """Create(s) uplink(s) with relative(s)"""

    def process_relatives(key):
        """Handling Relations based on key"""

        if key in data[zettel]:
            if type(data[zettel][key]) is list:
                for item in data[zettel][key]:
                    mmgraphviz.dot.node(item, data[item]["title"])
                    mmgraphviz.dot.edge(zettel, item)
                    uplink_relatives(item, data, filename)
            else:
                mmgraphviz.dot.node(data[zettel][key], data[data[zettel][key]]["title"])
                mmgraphviz.dot.edge(zettel, data[zettel][key])

    process_relatives("folge")
    process_relatives("subordinates")
    mmgraphviz.dot.render(f'doctest-output/{filename}.gv')


def create_mindmap(main_node, data):
    """Create MindMap of selected zettel."""

    if main_node not in data:
        raise LookupError("Die von Ihnen angegebene ID wurde im System nicht gefunden")

    mmgraphviz.dot.node(main_node, data[main_node]["title"])
    uplink_relatives(main_node, data, data[main_node]["title"])

