#!/bin/bash

# Update package lists
echo "Updating package lists..."
sudo apt-get update

# Check if Python 3.x is installed, if not, install it
if ! command -v python3 &> /dev/null; then
    echo "Python 3.x is not installed. Installing..."
    sudo apt-get install -y python3
else
    echo "Python 3.x is already installed."
fi

# Check if Graphviz is installed, if not, install it
if ! dpkg -l | grep -qw graphviz; then
    echo "Graphviz is not installed. Installing..."
    sudo apt-get install -y graphviz
else
    echo "Graphviz is already installed."
fi

# Clone the git repository
echo "Cloning the git repository..."
git clone https://gitlab.com/loewensteiner/swt-mindmap.git

# Navigate to the project directory
cd swt-mindmap || exit

# Create and activate a virtual environment
echo "Creating and activating a virtual environment..."
python3 -m venv venv
source venv/bin/activate

# Install Python requirements
echo "Installing Python requirements..."
pip install -r requirements.txt

# Run the main.py script
echo "Running the main.py script..."
python main.py

echo "Please enter an ID so that a PDF with the selected topic appears in the 'doctest-output' folder."

# Deactivate the virtual environment when done
# deactivate
