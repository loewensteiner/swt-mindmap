#!/bin/bash

# Check if Homebrew is installed, if not, install it
if ! command -v brew &> /dev/null; then
    echo "Homebrew is not installed. Installing..."
    /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
else
    echo "Homebrew is already installed."
fi

# Check if Python 3.x is installed
if ! command -v python3 &> /dev/null; then
    echo "Python 3.x is not installed. Please install it."
    exit 1
fi

# Check if Graphviz is installed via Homebrew, if not, install it
if ! brew list graphviz &> /dev/null; then
    echo "Graphviz is not installed. Installing..."
    brew install graphviz
else
    echo "Graphviz is already installed."
fi

# Clone the git repository
echo "Cloning the git repository..."
git clone https://gitlab.com/loewensteiner/swt-mindmap.git

# Navigate to the project directory
cd swt-mindmap

# Create and activate a virtual environment
echo "Creating and activating a virtual environment..."
python3 -m venv venv
source venv/bin/activate

# Install Python requirements
pip install -r requirements.txt

# Run the main.py script
echo "Running the main.py script..."
python main.py

echo "Please enter an ID so that a PDF with the selected topic appears in the 'doctest-output' folder."

# Deactivate the virtual environment when done
# deactivate
